package com.decamp.randomnumber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RandomNumberApplication {

	public static void main(String[] args) {
		SpringApplication.run(RandomNumberApplication.class, args);
	//khai báo đối tượng thuộc class CRandomNumber
	CRandomNumber randomNumber = new CRandomNumber();
	//In số ngẫy nhiê ra console
	System.out.println(randomNumber.randomDoubleNumber());
	System.out.println(randomNumber.randomIntNumber());
	}

}
